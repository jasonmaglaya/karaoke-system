var Room = require("../models/room");
var User = require("../models/user");
var Video = require("../models/video");
var {google} = require("googleapis");
var youtube = google.youtube({ version: "v3", auth: "AIzaSyBbu4DbcOf_7I9mt6TFQeLH9ugNlzlgPa4" });

module.exports = function(app, io) {
    var DEFAULT_VIDEO = "YgqH-LoLhug";

    // Find room
    app.get("/rooms/find", function(req, res) {
        var keyword = req.query.keyword.toString().trim().toLowerCase();
        var longitude = Number(req.query.lon.toString().trim());
        var latitude = Number(req.query.lat.toString().trim());
        var page = Number(req.query.page);

        Room.aggregate(
            [
                { "$geoNear": {
                    "near": {
                    "type": "Point",
                        "coordinates": [longitude, latitude]
                    },
                    "distanceField": "dist",
                    "spherical": true
                    }                
                },
                { "$project": {"_id": 0, "roomId": 1, "name": 1, "description": 1, "createdBy": 1, "i": { $indexOfCP: [ { $toLower: "$name" }, keyword ] } } },
                { "$match" : { "i" : { $gte: 0 } } },
                { "$sort": { "dist": 1 } }
            ],
            function(err,rooms) {
                if (err) {
                    throw err;
                }

                res.json(rooms);
            }
        );
    });

    //Register socket
    app.post("/rooms/register", isLoggedIn, function(req, res) {
        if (!req.body.socketId || !req.body.roomId) {
            res.json({ error: "Invalid data" });
            return;
        }

        var socketId = req.body.socketId.toString().trim();
        var roomId = req.body.roomId.toString().trim();
        var userId = req.user.id;

        User.findOne({ _id: userId }, function(error, user) {
            if (error) {
                console.log(error);
                res.json({ error });
                return;
            }

            if (!user) {
                res.json({ error: "User does not exists" });
                return;
            }

            user.socketId = socketId;

            user.save(function(error, user){
                if (error) {
                    console.log(error);
                    res.json({ error });
                    return;
                }

                Room.findOne({ roomId }, function(error, room) {
                    if (error) {
                        console.log(error);
                        res.json({ error });
                        return;
                    }

                    if(!room) {
                        res.json({ error: "Room does not exists" });
                        return;
                    }

                    if (room.guests.indexOf(userId) < 0) {
                        res.json({ error: "You are not a guest of this room" });
                        return;
                    }

                    var query = User.find({_id: { $in: room.guests } }).select("displayName -_id");
                    query.exec(function(error, results){
                        if (error) {
                            console.log(error);
                            res.json({ error });
                            return;
                        }

                        var guests = [];
                        results.map(x => {
                            guests.push(x.displayName);
                        });

                        res.json({
                            password: room.password,
                            roomName: room.name,
                            nowPlaying: room.nowPlaying,
                            reserved: room.reserved,
                            favorites: user.favorites,
                            guests: guests,
                            isHost: room.host === user.id
                        });
                    });
                });
            });
        });
    });

    //Find songs
    app.get("/rooms/songs/find", isLoggedIn, function(req, res) {
        var keyword = req.query.keyword.toString().trim().toLowerCase() + ' karaoke';
        
        youtube.search.list({
            part: "snippet",
            q: keyword,
            type: "video",
            videoEmbeddable: "true",
            maxResults: "25"
        }).then(function (results) {
            var videos = [];
            
            results.data.items.map(x => {                
                var v = new Video();
                v.videoId = x.id.videoId;
                v.title = x.snippet.title;
                v.videoType = "song";
                v.channel = x.snippet.channelTitle;
                v.thumbnail = x.snippet.thumbnails.high.url;

                videos.push(v);
            });

            res.json(videos);
        })
        .catch(function(err){
            res.send(err);
        });
    });

    // Reserve song
    app.post("/rooms/songs/reserve", isLoggedIn, function(req, res) {
        if (!req.body.video || !req.body.roomId) {
            res.json({ error: "Invalid data" });
            return;
        }

        var roomId = req.body.roomId.toString().trim();
        var video = req.body.video;
        var userId = req.user.id;

        Room.findOne({ roomId }, function(error, room) {
            if (error) {
                console.log(error);
                res.json({ error: "Something wrong" });                
                return;
            }

            if(!room) {
                res.json({ error: "Room does not exists" });
                return;
            }

            // if user is not a guest
            if (room.guests.indexOf(userId) < 0) {
                res.json({ error: "You are not a guest of this room" });
                return;
            }

            // if user is reserving the same song as previous
            if(room.reserved.length > 0) {
                var last = room.reserved[room.reserved.length - 1];
                if (last.videoId === video.videoId && last.singerUserId === userId) {
                    res.json({ error: "This song is already reserved" });
                    return;
                }
            }

            User.findOne({ _id: userId }, function(error, user) {
                if (error) {
                    console.log(error);
                    res.json({ error });
                    return;
                }

                if (!user) {
                    res.json({ error: "User does not exists" });
                    return;
                }

                var v = new Video();
                v.videoId = video.videoId;
                v.videoType = "song";
                v.title = video.title;
                v.channel = video.channel;
                v.thumbnail = video.thumbnail;
                v.singer = user.displayName;
                v.singerUserId = user.id;

                if(room.reserved.length === 0 && !room.nowPlaying) {
                    room.nowPlaying = v;
                    io.to(room.roomId).emit("onNowPlayingChanged", v);
                }
                else {
                    room.reserved.push(v);
                    io.to(room.roomId).emit("onReservedChanged", room.reserved);
                }

                room.save(function(error){
                    if (error) {
                        console.log(error);
                        res.json({ error: "Something wrong" });
                        return;
                    }

                    res.json({});
                });
            });
        });
    });
    
    // Remove reserved song
    app.post("/rooms/songs/remove", isLoggedIn, function(req, res) {
        if (!req.body.videoId || !req.body.roomId) {
            res.json({ error: "Invalid data" });
            return;
        }

        var videoId = req.body.videoId.toString().trim();
        var roomId = req.body.roomId.toString().trim();
        var userId = req.user.id;

        Room.findOne({ roomId }, function(error, room) {
            if (error) {                
                console.log(error);
                res.json({ error: "Something wrong" });
                return;
            }

            if (!room) {
                res.json({ error: "Room does not exists" });
                return;
            }

            // if user is not a guest
            if (room.guests.indexOf(userId) < 0) {
                res.json({ error: "You are not a guest of this room" });
                return;
            }

            // if user is not a guest
            if (room.host !== userId) {
                res.json({ error: "You can not remove this" });
                return;
            }

            var video = room.reserved.find(x => x.videoId === videoId);
            var i = room.reserved.indexOf(video);
            if (i > -1) {
                room.reserved.splice(i, 1);
            }

            room.save(function(error){
                if (error) {
                    console.log(error);
                    res.json({ error: "Something wrong" });
                    return;
                }

                io.to(room.roomId).emit("onReservedChanged", room.reserved);

                res.json({});
            });
        });
    });

    // Add favorite
    app.post("/favorites/add", isLoggedIn, function(req, res) {
        if (!req.body.video) {
            res.json({ error: "Invalid data" });
            return;
        }

        var video = req.body.video;
        var userId = req.user.id;

        User.findOne({ _id: userId }, function(error, user) {
            if (error) {
                console.log(error);
                res.json({ error });
                return;
            }

            if (!user) {
                res.json({ error: "User does not exists" });
                return;
            }

            if(user.favorites.find(x => x.videoId === video.videoId)) {
                res.json({ error: "The song is already in the favorites" });
                return;
            }

            video.videoType = "song";
            delete video.singer;
            delete video.singerUserId;

            user.favorites.push(video);

            user.save(function(error, user){
                if (error) {
                    console.log(error);
                    res.json({ error });
                    return;
                }

                res.json({ favorites: user.favorites });
            });
        });
    });

    // Remove favorite
    app.post("/favorites/remove", isLoggedIn, function(req, res) {
        if (!req.body.videoId) {
            res.json({ error: "Invalid data" });
            return;
        }

        var videoId= req.body.videoId;
        var userId = req.user.id;

        User.findOne({ _id: userId }, function(error, user) {
            if (error) {
                console.log(error);
                res.json({ error });
                return;
            }

            if (!user) {
                res.json({ error: "User does not exists" });
                return;
            }

            var fav = user.favorites.find(x => x.videoId === videoId);
            var i = user.favorites.indexOf(fav);
            if (i > -1) {
                user.favorites.splice(i, 1);
            }

            user.save(function(error, user){
                if (error) {
                    console.log(error);
                    res.json({ error });
                    return;
                }

                res.json({ favorites: user.favorites });
            });
        });
    });

    app.get("/rooms/create", isLoggedIn, function(req, res) {
        res.render("rooms/create.ejs", { message: "" });
    });

    app.post("/rooms/create", isLoggedIn, function(req, res) {
        var roomName = req.body.room.toString().trim();
        var description = req.body.description.toString().trim();
        var longitude = req.body.longitude.toString().trim();
        var latitude = req.body.latitude.toString().trim();
        var userId = req.user.id;

        process.nextTick(function() {
            Room.findOne({ "name": roomName }, function(err, room) {
                if (err) {
                    throw err;
                }

                if (room) {
                    res.render("rooms/create.ejs", { message: "Room name already exists" });
                    return;
                }

                room = new Room();                
                room.name = roomName;
                room.description = description;
                room.createdBy = res.locals.userName;
                room.location.coordinates = [Number(longitude), Number(latitude)];
                room.host = userId;
                room.guests.push(userId);                
                room.save(function(err) {
                    if (err) {
                        throw err;
                    }
                    res.redirect(`/rooms/${room.roomId}`);
                });
            });
        });
    });

    app.get("/rooms/:roomId", isLoggedIn, function(req, res) {
        if (!req.params.roomId) {
            res.send("Invalid data");
            return;
        }

        var roomId = req.params.roomId;
        var userId = req.user.id;

        Room.findOne({ "roomId": roomId }, function(err, room) {
            if (err) {                
                throw err;
            }

            if (!room) {
                res.redirect("/rooms");
                return;
            }

            if (room.guests.indexOf(userId) > -1) {
                res.render("rooms/room.ejs", { roomId });
            }
            else {
                res.render("rooms/enter.ejs", { roomId , message: "" });
            }
        });
    });

    app.post("/rooms/:roomId/enter", isLoggedIn, function(req, res) {
        if (!req.params.roomId || !req.body.password) {
            res.send("Invalid data");
            return;
        }

        var roomId = req.params.roomId;
        var password = req.body.password.toString().trim();

        Room.findOne({ "roomId": roomId }, function(err, room) {
            if (err) {
                throw err;
            }

            if (!room) {
                res.render("rooms/enter.ejs", { roomId, message: "Room does not exists" });
                return;
            }

            if (room.password !== password) {
                res.render("rooms/enter.ejs", { roomId, message: "Invalid passcode" });
                return;
            }
            
            if (room.guests.indexOf(req.user.id) > -1) {
                res.redirect(roomId);
                return;
            }
            
            if (room.guests.indexOf(req.user.id) === -1) {
                room.guests.push(req.user.id);
            }

            room.save(function(err) {
                if (err) {
                    throw err;
                }
                
                var userName = req.user.displayName || req.user.facebook.name || req.user.google.name || 
                                req.user.twitter.displayName || req.user.local.name || "A guest";

                var query = User.find({_id: { $in: room.guests } }).select("displayName -_id");
                query.exec(function(error, results){
                    if (error) {
                        console.log(error);
                        res.send({ error });
                        return;
                    }

                    var guests = [];
                    results.map(x => {
                        guests.push(x.displayName);
                    });

                    io.to(room.roomId).emit("onUserJoined", { message: `${userName} entered the room`, guests });
                });

                res.redirect(`/rooms/${roomId}`);
            });
        });
    });

    app.get("/rooms/:roomId/player", isLoggedIn, function(req, res) {
        if (!req.params.roomId) {
            res.send("Invalid data");
            return;
        }

        var roomId = req.params.roomId;
        var userId = req.user.id;

        Room.findOne({ "roomId": roomId }, function(err, room) {
            if (err) {                
                throw err;
            }

            if (!room) {
                res.redirect("/rooms");
                return;
            }

            if (room.host === userId) {
                if(!room.nowPlaying) {
                    room.nowPlaying = { videoId: DEFAULT_VIDEO, title: "" };
                }

                res.render("rooms/player.ejs", { roomId, nowPlaying: room.nowPlaying.videoId, nowPlayingTitle: room.nowPlaying.title });
            }
            else {
                res.redirect(`/rooms/${roomId}`);
            }
        });
    });

    app.post("/rooms/:roomId/player/pause", isLoggedIn, function(req, res) {
        if (!req.params.roomId) {
            res.json({ error: "Invalid data" });
            return;
        }

        var roomId = req.params.roomId;

        io.to(roomId).emit("onPause");

        res.json({});
    });

    app.post("/rooms/:roomId/player/next", isLoggedIn, function(req, res) {
        if (!req.params.roomId) {
            res.json({ error: "Invalid data" });
            return;
        }

        var roomId = req.params.roomId;

        Room.findOne({ roomId }, function(error, room) {
            if (error) {
                console.log(error);
                res.json({ error: "Something wrong" });
                return;
            }

            if (!room) {
                res.json({ error: "Room does not exists" });
                return;
            }

            var video;
            if(room.reserved.length === 0) {                
                video = { videoId: DEFAULT_VIDEO, title: "", thumbnail: "/img/select_song.png" };
                room.nowPlaying = null;
            }
            else{
                video = room.reserved[0];
                room.nowPlaying = video;
            }

            io.to(room.roomId).emit("onNowPlayingChanged", video);
            
            var i = room.reserved.indexOf(video);
            if (i > -1) {
                room.reserved.splice(i, 1);
            }

            room.save(function(error){
                if (error) {
                    console.log(error);
                    res.json({ error: "Something wrong" });
                    return;
                }
                
                io.to(room.roomId).emit("onReservedChanged", room.reserved);

                res.json({});
            });
        });
    });

    function isLoggedIn(req, res, next) {

        // if user is authenticated in the session, carry on 
        if (req.isAuthenticated())
            return next();
    
        // if they aren"t redirect them to the home page    
        res.redirect("/login?returnUrl=" + req.url);
    }
};