module.exports = function(app, passport, io) {
    app.use(function(req, res, next) {
        res.locals.isAuthenticated = req.isAuthenticated();
        if (res.locals.isAuthenticated) {
            res.locals.userName = req.user.local.name || req.user.facebook.name || req.user.google.name || 
                                req.user.twitter.displayName || '';
        }
        else {
            res.locals.userName = '';
        }

        if(req.query.returnUrl) {
            res.locals.returnUrl = 'returnUrl=' + req.query.returnUrl;
        }
        else {
            res.locals.returnUrl = '';
        }

        next();
    });

    // =====================================
    // HOME PAGE (with login links) ========
    // =====================================
    app.get('/', function(req, res) {
        res.redirect('/rooms');
    });

    app.get('/rooms', function(req, res) {
        res.render('rooms/index.ejs');
    });

    // =====================================
    // LOGIN ===============================
    // =====================================
    // show the login form
    app.get('/login', function(req, res) {
        // render the page and pass in any flash data if it exists
        res.render('login.ejs', { message: req.flash('loginMessage') }); 
    });

    // process the login form
    app.post('/login', function (req, res, next) {        
        passport.authenticate('local-login', {
            successRedirect : '/', // redirect to the secure profile section
            failureRedirect : req.url, // redirect back to the signup page if there is an error
            failureFlash : true, // allow flash messages
            successReturnToOrRedirect: req.query.returnUrl
        })(req, res, next);
    });


    // =====================================
    // CONNECT LOCAL ACCOUNT ================
    // =====================================    
    app.get('/connect/local', function(req, res) {
        res.render('connect-local.ejs', { message: req.flash('signupMessage') });
    });

    app.post('/connect/local', passport.authenticate('local-signup', {
        successRedirect : '/profile', // redirect to the secure profile section
        failureRedirect : '/connect/local', // redirect back to the signup page if there is an error
        failureFlash : true // allow flash messages
    }));
    

    // =====================================
    // SIGNUP ==============================
    // =====================================
    // show the signup form
    app.get('/signup', function(req, res) {
        // render the page and pass in any flash data if it exists        
        res.render('signup.ejs', { message: req.flash('signupMessage') });
    });

    // process the signup form
    app.post('/signup', function(req, res, next) {        
        passport.authenticate('local-signup', {
            successRedirect : '/', // redirect to the secure profile section
            failureRedirect : req.url, // redirect back to the signup page if there is an error
            failureFlash : true, // allow flash messages
            successReturnToOrRedirect: req.query.returnUrl
        })(req, res, next);
    });


    // =====================================
    // PROFILE SECTION =====================
    // =====================================
    // we will want this protected so you have to be logged in to visit
    // we will use route middleware to verify this (the isLoggedIn function)
    app.get('/favorites', isLoggedIn, function(req, res) {        
        res.render('profile.ejs', {
            user : req.user // get the user out of session and pass to template
        });
    });

    
    // =====================================
    // LOGOUT ==============================
    // =====================================
    app.get('/logout', function(req, res) {
        req.logout();
        res.redirect('/');
    });


    // =====================================
    // FACEBOOK ROUTES =====================
    // =====================================
    // route for facebook authentication and login
    app.get('/auth/facebook', passport.authenticate('facebook', { 
        scope : ['public_profile', 'email']
    }));
  
    // handle the callback after facebook has authenticated the user
    app.get('/auth/facebook/callback', passport.authenticate('facebook', {
        successRedirect : '/',
        failureRedirect : '/'
    }));
    
    // send to facebook to do the authentication
    app.get('/connect/facebook', passport.authorize('facebook', { 
        scope : ['public_profile', 'email'] 
    }));

    // handle the callback after facebook has authorized the user
    app.get('/connect/facebook/callback', passport.authorize('facebook', {
        successRedirect : '/',
        failureRedirect : '/'
    }));


    // =====================================
    // TWITTER ROUTES ======================
    // =====================================
    // route for twitter authentication and login
    app.get('/auth/twitter', passport.authenticate('twitter'));

    // handle the callback after twitter has authenticated the user
    app.get('/auth/twitter/callback', passport.authenticate('twitter', {
        successRedirect : '/',
        failureRedirect : '/'
    }));

    // send to twitter to do the authentication
    app.get('/connect/twitter', passport.authorize('twitter', {
        scope : 'email' 
    }));

    // handle the callback after twitter has authorized the user
    app.get('/connect/twitter/callback', passport.authorize('twitter', {
        successRedirect : '/',
        failureRedirect : '/'
    }));


    // =====================================
    // GOOGLE ROUTES =======================
    // =====================================
    // send to google to do the authentication
    // profile gets us their basic information including their name
    // email gets their emails
    app.get('/auth/google', passport.authenticate('google', { 
        scope : ['profile', 'email']
    }));

    // the callback after google has authenticated the user
    app.get('/auth/google/callback', passport.authenticate('google', {
        successRedirect : '/',
        failureRedirect : '/'
    }));

    // send to google to do the authentication
    app.get('/connect/google', passport.authorize('google', {
        scope : ['profile', 'email']
    }));

    // the callback after google has authorized the user
    app.get('/connect/google/callback', passport.authorize('google', {
        successRedirect : '/',
        failureRedirect : '/'
    }));

    // =====================================
    // KARAOKE ROOMS =======================
    // =====================================

    // Room Routes    
    require("./rooms.js")(app, io);
};

// route middleware to make sure a user is logged in
function isLoggedIn(req, res, next) {

    // if user is authenticated in the session, carry on 
    if (req.isAuthenticated())
        return next();

    // if they aren't redirect them to the home page    
    res.redirect('/login?returnUrl=' + req.url);
}