var User = require('../app/models/user');
var Room = require('../app/models/room');

module.exports = function(socket) {
    const _rooms = [];
    const _guests = [];
    const _timeouts = {};

    var io = socket.of('/karaokey');

    io.on('connection', (socket) => {
        console.log(socket.id);
        socket.on('register', (data, next) => {
            User.findById(data.userId, function(error, user){
                if (error) {
                    next({ error });
                }

                if (!user) {
                    next({ error: 'User does not exists' });
                }

                user.socketId = socket.id;

                user.save(function(error){
                    if (error) {
                        next({ error });
                        return;
                    }

                    next();
                });
            });
        });

        socket.on('disconnect', () => {
            User.findOne({ 'socketId': socket.id }, function(error, user){
                if (error) {
                    throw error;
                }

                if (!user) {
                    return;
                }

                const timeout = setTimeout(() => {
                    if (!user.socketId) {						
                        leaveRoom(user.id);
                    }

                    delete _timeouts[user.id];
                }, 10000);

                if(_timeouts[user.id]) {
                    clearTimeout(_timeouts[user.id]);
                }

                _timeouts[user.id] = timeout;
            });
        });
        
        socket.on('getRooms', (data, next) => {
            next({ rooms: _rooms });
        });
        
        socket.on('createRoom', (data, next) => {
            const roomId = guid();
            
            if (_rooms.filter(x => x.id === roomId || x.name === data.roomName).length > 0) {			
                if (next) {
                    next({error: 'Room already exists'});
                }

                return;
            }
            
            leaveRoom(data.guestId); // to ensure that a guest cam only join one room at a time

            const guest = _guests.find(x => x.id === data.guestId);
            if(!guest) {
                next({ error: 'Guest does not exist' });
                return;
            }

            guest.name = data.name;

            const room = {
                id: roomId,
                name: data.roomName,
                createdBy: guest.name,
                createdByGuestId: guest.id,
                host: guest.id,
                guests: [guest.id],
                passcode: data.passcode
            };

            _rooms.push(room);
            
            sendToAll('onRoomCreated', { id: room.id, name: room.name });

            if (next) {
                next({id: room.id, name: room.name});
            }
        });
        
        socket.on('joinRoom', (data, next) => {
            const room = _rooms.find(x => x.id === data.roomId);
            if (!room) {
                if (next) {
                    next({ error: 'Room does not exist anymore' });
                    return;
                }
            }
        
            if (room.guests.indexOf(data.guestId) >= 0) {
                if (next) {
                    next({ error: 'You already joined the room' });
                    return;
                }
            }

            if (room.passcode !== data.passcode) {
                if (next) {
                    next({ error: 'Passcode is incorrect' });
                    return;
                }
            }

            leaveRoom(data.guestId); // to ensure that a guest cam only join one room at a time

            const guest = _guests.find(x => x.id === data.guestId);

            if(!guest) {
                next({ error: 'Guest does not exist' });
                return;
            }

            guest.name = data.name;

            sendToRoom(room, 'onGuestJoined', { name: guest.name });

            room.guests.push(guest.id);
            
            if (next) {
                next({ success: true });
            }
        });
        
        socket.on('leaveRoom', (data, next) => {
            const guest = _guests.find(x => x.id === data.guestId);
            if(!guest) {
                next({ error: 'Guest does not exist' });
                return;
            }

            leaveRoom(guest.id);		

            if (next) {
                next({ success: true });
            }
        });

        function sendToAll(eventName, data) {		
            io.emit(eventName, data);
        }

        function sendToRoom(room, eventName, data) {
            room.guests.map(x => {
                const guest = _guests.find(g => g.id == x);
                if (guest && guest.connected) {
                    guest.socket.emit(eventName, data);
                }
            });
        }

        function guid() {
            function s4() {
                return Math.floor((1 + Math.random()) * 0x10000)
                    .toString(16)
                    .substring(1);
            }
            return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
        }
        
        function leaveRoom(userId) {
            const guest = _guests.find(x => x.id === userId);
            if (!guest) {
                return;
            }

            _rooms.filter(x => x.guests.indexOf(userId) > -1).map(room => {
                // remove guest from the room
                let i = room.guests.indexOf(userId);
                if (i > -1) {
                    room.guests.splice(i, 1);
                }
        
                sendToRoom(room, 'onGuestLeft', { name: guest.name });
        
                // remove room if there is no more guest left
                if (room.guests.length <= 0) {
                    const timeout = setTimeout(() => {
                            if (room.guests.length <= 0) {
                                i = _rooms.indexOf(room);
                                if (i > -1) {
                                    _rooms.splice(i, 1);
                                }
                                sendToAll('onRoomRemoved', {});
                            }

                            delete _timeouts[room.id];
                        }, 10000);
                    
                    if (_timeouts[room.id]) {
                        clearTimeout(_timeouts[room.id]);
                    }

                    _timeouts[room.id] = timeout;
                }
                else {
                    if (room.host === guestId) {
                        room.host = room.guests[0]; // replace host
                        sendToRoom(room, 'onHostChanged', { guestId: room.host });
                    }
                }
            });
        }
    });
};