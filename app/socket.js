var User = require("../app/models/user");
var Room = require("../app/models/room");

module.exports = function(io) {
    const _timeouts = {};
    
    io.on("connection", (socket) => {
        // Join
        socket.on("join", (roomId, next) => {
            socket.join(roomId);
            next();
        });

        // Disconnect
        socket.on("disconnect", () => {
            User.findOne({ "socketId": socket.id }, function(err, user) {                
                if (err) {
                    throw err;
                }

                if (!user) {
                    return;
                }

                user.socketId = null;

                user.save(function(err, user){
                    const timeout = setTimeout(() => {
                        User.findById(user.id, function(err, user) {
                            if (err) {
                                throw err;
                            }

                            if (!user.socketId) {
                                leaveRoom(user);
                            }
        
                            delete _timeouts[user.id];
                        });
                    }, 10000);
    
                    if(_timeouts[user.id]) {
                        clearTimeout(_timeouts[user.id]);
                    }
    
                    _timeouts[user.id] = timeout;
                });
            });
        });

        function leaveRoom(user) {
            Room.find({ "$where": `this.guests.indexOf("${user.id}") > -1` }, function(err, rooms) {
                if (err) {
                    throw err;
                }
    
                if (!rooms) {
                    return;
                }
    
                rooms.map(x => {
                    var i = x.guests.indexOf(user.id);
                    if (i > -1) {
                        x.guests.splice(i, 1);
                    }

                    if (x.guests.length === 0) {
                        Room.findByIdAndRemove(x.id).exec();
                        return;
                    }

                    if (x.host === user.id) {
                        x.host = x.guests[0]; // replace host
                        io.to(x.roomId).emit("onHostChanged", x.host);
                    }

                    x.save(function(err) {
                        if (err) {
                            throw err;
                        }
                    });

                    socket.leave(x.roomId);

                    var query = User.find({_id: { $in: x.guests } }).select("displayName -_id");
                    query.exec(function(error, results){
                        if (error) {
                            console.log(error);
                            if(next) {
                                next({ error });
                            }
                            return;
                        }

                        var guests = [];
                        results.map(r => {
                            guests.push(r.displayName);
                        });
                        
                        io.to(x.roomId).emit("onUserLeft", { message: `${getUserName(user)} left the room`, guests });
                    });
                });
            });
        }

        function getUserName(user) {
            return user.displayName || user.facebook.name || user.google.name || 
            user.twitter.displayName || user.local.name || "A guest";
        }
    });
};