var mongoose = require("mongoose");

var videoSchema = mongoose.Schema({
    videoId: String,
    videoType: String,
    title : String,
    channel: String,
    thumbnail: String,
    singer: String,
    singerUserId: String
});

module.exports = mongoose.model("Video", videoSchema);
