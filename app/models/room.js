var mongoose = require("mongoose");
var Video = require("./video");

var roomSchema = mongoose.Schema({
    roomId: String,
    name : String,
    description: String,
    createdBy: String,
    password : String,
    location: { "type": { type: String, enum: "Point", default: "Point" }, coordinates: { type: [Number], default: [0, 0] } },
    host: String,
    guests : [String],
    reserved: [Video.schema],
    nowPlaying: Video.schema
});

roomSchema.index({ location: "2dsphere" });

function generatePassword() {
    return Math.floor(1000 + Math.random() * 9000);
}

function generateRoomId() {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
  
    for (var i = 0; i < 15; i++)
      text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
}

roomSchema.pre("save",function(next){
	if(!this.password) {
        this.password = generatePassword();
    }

    if(!this.roomId) {
        this.roomId = generateRoomId();
    }

	next();
});

module.exports = mongoose.model("Room", roomSchema);