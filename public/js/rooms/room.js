$.LoadingOverlay("show");
var player;
var previewVideo;

function onYouTubeIframeAPIReady() {
    initPlayer();
}

function initPlayer() {
    player = new YT.Player("player", 
    { 
        playerVars: { disablekb: 0, showinfo: 0, fs: 0 },
        events: {
            "onStateChange": function(state) {
                if (state.data === 0) {
                    $("#preview").modal("hide");
                }
            }
        }
    });
}

var View = function() {
    var self = this;

    self.roomName = ko.observable("");
    self.password = ko.observable("");
    self.nowPlaying = ko.observable({ title: "", thumbnail: "/img/select_song.png" });
    self.keyword = ko.observable("");
    self.results = ko.observableArray([]);
    self.reserved = ko.observableArray([]);
    self.favorites = ko.observableArray([]);
    self.guests = ko.observableArray([]);
    self.isHost = ko.observable(false);
    self.showPopupReserveBtn = ko.observable(true);

    self.showClearButton = ko.computed(function() {
        return self.keyword().toString().trim().length > 0;
    });
    
    self.playPause = function() {
        $.ajax({
            url: "/rooms/" + roomId + "/player/pause",
            type: "post",
            dataType: "json",
            contentType: "application/json",
            data: JSON.stringify({ }),
            success: function (data) {
                if(data.error) {
                    toastr.error(data.error);
                }                
            },
            error: fnError
        });
    };

    self.next = function() {
        $.ajax({
            url: "/rooms/" + roomId + "/player/next",
            type: "post",
            dataType: "json",
            contentType: "application/json",
            data: JSON.stringify({ }),
            success: function (data) {
                if(data.error) {
                    toastr.error(data.error);
                }                
            },
            error: fnError
        });
    };

    self.addFavorite = function(video) {
        if(!video || !video.videoId) {
            return;
        }

        $.ajax({
            url: "/favorites/add",
            type: "post",
            dataType: "json",
            contentType: "application/json",
            data: JSON.stringify({ video }),
            success: function (data) {
                if(data.error) {
                    toastr.error(data.error);
                }
                else {
                    toastr.success("Added to favorites");
                    vm.favorites(data.favorites);
                }
            },
            error: fnError
        });
    };

    self.removeFavorite = function(videoId) {
        if(!videoId) {
            return;
        }

        $("#myFavorites").LoadingOverlay("show");
        
        $.ajax({
            url: "/favorites/remove",
            type: "post",
            dataType: "json",
            contentType: "application/json",
            data: JSON.stringify({ videoId }),
            success: function (data) {
                if(data.error) {
                    toastr.error(data.error);
                }
                else {
                    vm.favorites(data.favorites);
                }

                $("#myFavorites").LoadingOverlay("hide", true);
            },
            error: fnError
        });
    };

    var playerWindow;
    self.openPlayer = function() {
        if(!playerWindow) {
            playerWindow = window.open("/rooms/" + roomId + "/player");
        }
        playerWindow.focus();
    };

    self.clear = function() {
        self.keyword("");
        self.results([]);
    };

    self.search = function() {
        var keyword = this.keyword();
        if(keyword.toString().trim().length === 0) {
            return;
        }
        
        $("#results").LoadingOverlay("show");

        $.getJSON("/rooms/songs/find", {
            keyword
        })
        .done(function(data) {
            self.results(data);
            $("#results").LoadingOverlay("hide", true);
        })
        .fail(function(err) {
            console.log(err);
        });
    };

    self.reserve = function(video) {
        if(!video) {
            return;
        }

        $.ajax({
            url: "/rooms/songs/reserve",
            type: "post",
            dataType: "json",
            contentType: "application/json",
            data: JSON.stringify({ roomId, video }),
            success: function (data) {
                if(data.error) {
                    toastr.error(data.error);
                }
                else {
                    toastr.success("Reserved");
                }
            },
            error: fnError
        });
    };

    self.removeReserved = function(videoId) {
        if(!videoId) {
            return;
        }

        $("#reservedList").LoadingOverlay("show");

        $.ajax({
            url: "/rooms/songs/remove",
            type: "post",
            dataType: "json",
            contentType: "application/json",
            data: JSON.stringify({ roomId, videoId }),
            success: function (data) {
                if(data.error) {
                    toastr.error(data.error);
                }

                $("#reservedList").LoadingOverlay("hide", true);
            },
            error: fnError
        });
    };

    self.preview = function(video, showPopupReserveBtn) {
        if(!player) {
            initPlayer();
        }

        previewVideo = video;
        player.loadVideoById({ videoId: video.videoId });
        self.showPopupReserveBtn(showPopupReserveBtn);
        $("#preview").modal();
    };
};

function fnError(xhr, resp, text) {
    console.log(xhr, resp, text);
    toastr.error(text);
    $("#reservedList").LoadingOverlay("hide", true);
}

var vm = new View();
ko.applyBindings(vm);

var socket = io();
socket.on("error", console.error.bind(console));

socket.on("onUserJoined", function (data) {
    vm.guests(data.guests);
    toastr.info(data.message);
});

socket.on("onUserLeft", function (data) {
    vm.guests(data.guests);
    toastr.info(data.message);
});

socket.on("onReservedChanged", function(reserved) {
    vm.reserved(reserved);
});

socket.on("onNowPlayingChanged", function(video) {
    if(!video) {
        video = { title: "", thumbnail: "/img/select_song.png" };
    }
    vm.nowPlaying(video);
});

toastr.options = {
    "closeButton": true,
    "positionClass": "toast-bottom-right"
}

$("#preview").on("hide.bs.modal", function (e) {
    previewVideo = null;
    player.stopVideo();    
});

$("a[href='#search']").on("shown.bs.tab", function (e) {
    $("#txtKeyword").focus();
});

window.addEventListener("beforeunload", function (e) {
    var confirmationMessage = "Do you really want to leave?";

    (e || window.event).returnValue = confirmationMessage; //Gecko + IE

    return confirmationMessage; //Gecko + Webkit, Safari, Chrome etc.
});

socket.on("connect", function() {
    var socketId = socket.id;
    socket.emit("join", roomId, function(){
        $.ajax({
            url: "/rooms/register",
            type: "post",
            dataType: "json",
            contentType: "application/json",
            data: JSON.stringify({ socketId, roomId }),
            success: function (data) {
                if(data.error) {
                    toastr.error(data.error);
                }
                else {
                    vm.password(data.password);
                    vm.roomName(data.roomName);
                    
                    if(data.nowPlaying) {
                        vm.nowPlaying(data.nowPlaying);                        
                    }
                    else {
                        vm.nowPlaying({ title: "", thumbnail: "/img/select_song.png" });
                    }
    
                    vm.reserved(data.reserved);
                    vm.favorites(data.favorites);
                    vm.guests(data.guests);
                    vm.isHost(data.isHost);
                }
    
                $.LoadingOverlay("hide", true);
            },
            error: fnError
        });
    });
});

$(function() {
    if(!player) {
        initPlayer();
    }
});