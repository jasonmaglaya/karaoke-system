var lat = 0;
var lon = 0;

var View = function() {
    var self = this;

    self.keyword = ko.observable("");

    self.rooms = ko.observableArray([]);

    self.page = ko.observable(1);

    self.showClearButton = ko.computed(function() {
        return self.keyword().toString().trim().length > 0;
    });

    self.clear = function() {
        self.keyword("");
        self.findRoom();
    };

    self.findRoom = function() {
        $("#results").LoadingOverlay("show");

        $.getJSON("/rooms/find", {
            keyword: this.keyword(),
            lon,
            lat,
            page: this.page()
        })
        .done(function(data) {
            self.rooms(data);
            $("#results").LoadingOverlay("hide", true);
        })
        .fail(function(err) {
            console.log(err);
        });
    };
};

var vm = new View();
ko.applyBindings(vm);

vm.findRoom();

navigator.geolocation.getCurrentPosition(function(position) {
    lon = position.coords.longitude;
    lat = position.coords.latitude;
    vm.findRoom();
});

ko.bindingHandlers.enterKey = {
    init: function (element, valueAccessor, allBindings, data, context) {
        var wrapper = function (data, event) {
            if (event.keyCode === 13) {
                valueAccessor().call(this, data, event);
            }
        };
        ko.applyBindingsToNode(element, { event: { keyup: wrapper } }, context);
    }
};