var player;
function onYouTubeIframeAPIReady() {
    initPlayer();
}

function initPlayer() {
    player = new YT.Player("player", 
    {
        playerVars: { disablekb: 1, showinfo: 0 },
        events: {
            "onReady": onPlayerReady,
            "onStateChange": function(state) {
                if (state.data === 0) { // ended
                    $.ajax({
                        url: "/rooms/" + roomId + "/player/next",
                        type: "post",
                        dataType: "json",
                        contentType: "application/json",
                        data: JSON.stringify({ }),
                        success: function (data) {
                            if(data.error) {
                                toastr.error(data.error);
                            }
                        }
                    });
                }
            }
        }
    });
}

function onPlayerReady(event) {
    if(!player) {
        initPlayer();
    }

    vm.title(nowPlayingTitle);
    player.loadVideoById({ videoId: nowPlaying });
}

var View = function() {
    var self = this;

    self.title = ko.observable("");
    self.paused = ko.observable(false);
};

var vm = new View();
ko.applyBindings(vm);

var socket = io();
socket.on("error", console.error.bind(console));

socket.on("onNowPlayingChanged", function(video) {
    if(video) {        
        vm.title(video.title);
        player.loadVideoById({ videoId: video.videoId });
    }
});

socket.on("onPause", function() {
    if(vm.paused()) {
        player.playVideo();
    }
    else {
        player.pauseVideo();
    }

    vm.paused(!vm.paused());
});

socket.emit("join", roomId, function(){});

$(function() {
    if(!player) {
        initPlayer();
    }
});