var lat = 0;
var long = 0;

navigator.geolocation.getCurrentPosition(function(position) {
    lat = position.coords.latitude;
    long = position.coords.longitude;

    $('#txtLongitude').val(long);
    $('#txtLatitude').val(lat);
});